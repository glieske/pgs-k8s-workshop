#!/usr/bin/env bash
docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com

docker build -t registry.gitlab.com/glieske/pgs-k8s-workshop:latest .
docker push registry.gitlab.com/glieske/pgs-k8s-workshop
